import sys, json
import importlib
import os
gpm_root = f'/{"/".join(os.path.realpath(__file__).split("/")[1:-1])}'
gpm_lib = f'{gpm_root}/pylib/'
sys.path.append(gpm_lib)
import arrow as arrow
from colorama import Fore, Back, Style

class GPMLogger:
    @staticmethod
    def write(tag, text):
        #dt = arrow.utcnow().format('YYYY-MM-DD HH:mm')
        print(tag, text)

    @staticmethod
    def log(*text):
        #dt = arrow.utcnow().format('YYYY-MM-DD HH:mm')
        #print(dt, f"[{Fore.BLUE}LOG{Style.RESET_ALL}]", text)
        GPMLogger.write(f"[{Fore.BLUE}LOG{Style.RESET_ALL}]", " ".join(text))

    @staticmethod
    def info(*text):
        #dt = arrow.utcnow().format('YYYY-MM-DD HH:mm')
        #print(dt, f"[{Fore.YELLOW}INFO{Style.RESET_ALL}]", text)
        GPMLogger.write(f"[{Fore.YELLOW}INFO{Style.RESET_ALL}]", " ".join(text))

    @staticmethod
    def error(*text):
        #dt = arrow.utcnow().format('YYYY-MM-DD HH:mm')
        #print(dt, f"[{Fore.RED}ERROR{Style.RESET_ALL}]", text)
        GPMLogger.write(f"[{Fore.RED}ERROR{Style.RESET_ALL}]", " ".join(text))

class GPM:
    def __init__(self):
        self.config = {}

    def inferFileExt(self, path):
        if "." in path:
            return path.split(".")[-1]
        else:
            return ""

    def installer(self, itype, ipath):
        """
        Main entrypoint installer function, to handle what to do with a path and how to integrate it.
        Dispatches to other smaller InstallerScripts to handle things.
        """
        pass

    def runner(self, path, param):
        """
        Run apps!
        """
        GPMLogger.info("Launching "+path)
        ext = self.inferFileExt(path)
        if ext == "":
            os.system(f"{path} {param}")
        else:
            if not self.config.get("launchers"): self.config["launchers"] = {}
            if ext.lower() in self.config["launchers"].keys() and self.config["launchers"][ext].get("executable"):
                GPMLogger.info("Launching in "+self.config["launchers"].get("name", "undefined"))
                os.system(f'{self.config["launchers"][ext]["executable"]} {path} {param}')
            else:
                GPMLogger.info("Could not find defined launcher. Attempting to execute anyway.")
                os.system(f"{path} {param}")

    def getConfig(self):
        if self.config != {}: return self.config
        with open(gpm_root+"/config.json", "r") as f:
            self.config = json.load(f)
            return self.config


    def setConfig(self):
        with open(gpm_root+"/config.json", "w+") as f:
            return json.dump(self.config, f, indent=4)

    def getAppDir(self):
        return gpm_root+"/"+self.config["_appDir"]

    def updateConfig(self, key, value):
        if key.startswith("launchers.") or key == "launchers":
            return GPMLogger.error("Launcher key can not be edited this way.")

        if "." in key:
            grp = key.split(".")[0]
            rkey = key.split(".")[1]
            if not self.config.get(grp):
                self.config[grp] = {}
            self.config[grp][rkey] = value
            self.setConfig()
        else:
            self.config[key] = value
            self.setConfig()

    def main(self, args):
        if len(args) == 0:
            return GPMLogger.error("No command given.")

        self.config = self.getConfig()
        cmd = args[0]

        if cmd == "set":
            if len(args) < 2:
                return GPMLogger.error("Not enough parameters.")

            val = " ".join(args[2:])
            if val.isdigit(): val = int(val)
            if val == "true": val = True
            if val == "false": val = False
            self.updateConfig(args[1], val)
            GPMLogger.info(args[1], "=", '"'+str(val)+'"')

        elif cmd == "get":
            if len(args) < 2:
                for k, v in self.config.items():
                    GPMLogger.info(k, "=", '"'+str(v)+'"')
                return

            if self.config.get(args[1]) != None:
                GPMLogger.info(args[1], "=", '"'+str(self.config[args[1]])+'"')
            else:
                GPMLogger.error("Invalid key.")
        
        elif cmd == "run" or cmd == "r":
            opts = []
            for filename in os.listdir(self.getAppDir()):
                if filename.lower().startswith(args[1]): 
                    opts.append(filename)
            
            if len(opts) == 1:
                self.runner(self.getAppDir()+opts[0], " ".join(args[2:]))
            elif len(opts) > 1:
                print("Ambiguous choice")
                for i, name in enumerate(opts):
                    print(i, name)
                f = int(input("> "))
                self.runner(self.getAppDir()+opts[f], " ".join(args[2:]))
            else:
                GPMLogger.error("No launcher found.")

        elif cmd == "install":
            if len(args) <= 1: 
                GPMLogger.info("file/f")
                GPMLogger.info("net/web/dl")
                GPMLogger.info("repofile/rf/repo")
                GPMLogger.info("github/git/gh/g")
                for filename in os.listdir(gpm_root+"/installScripts"):
                    if filename.endswith(".py"): 
                        GPMLogger.info(Fore.BLUE+filename.split(".")[0])
                return 
            data = self.getAppDir()
            itype = args[1]
            if itype in ["file", "f"]: #takes a file executable and creates a link in apps dir
                pass
            elif itype in ["net", "web", "dl"]: #downloads executable file, then passes to installer
                pass
            elif itype in ["repofile", "rf", "repo"]: #parses proprietary repofile format
                pass
            elif itype in ["github", "git", "gh", "g"]: #looks for releases, then passes to installer
                if len(args) <= 2: return GPMLogger.error("No repo identifier given.")

                if "/" not in args[2]: return GPMLogger.error("Format must be owner/repo.")
                url = f"https://api.github.com/repos/{args[2]}/releases"
                GPMLogger.log("Checking "+url)
            else:
                #look for InstallScripts for custom handlers
                for filename in os.listdir(gpm_root+"/installScripts"):
                    if filename.endswith(".py"): 
                        os.chdir(gpm_root+"/installScripts")
                        module = importlib.import_module("installScripts."+filename.split(".")[0])
                        module.installApp(" ".join(args[2:]), logger=GPMLogger, scriptDir = gpm_root)

        elif cmd == "list" or cmd == "ls":
            cats = {}
            for filename in os.listdir(self.getAppDir()):
                if self.config.get(filename) and self.config[filename].get("group"):
                    if not cats.get(self.config[filename]["group"]): cats[self.config[filename]["group"]] = []
                    cats[self.config[filename]["group"]].append(filename)
                else:
                    if not cats.get("undefined"): cats["undefined"] = []
                    cats["undefined"].append(filename)
            
            cats = dict(sorted(cats.items(), key=lambda p: p[0]))

            for group in cats.keys():
                if group not in self.config.get("_hidden", []) and group != "undefined":
                    print(" "+Fore.RED+group+Style.RESET_ALL)
                    for app in sorted(cats[group]):
                        if app not in self.config.get("_hidden", []):
                            print(app)

            if cats.get("undefined") and self.config.get("show_ungrouped", True):
                print(" "+Fore.RED+"No group"+Style.RESET_ALL)
                for app in sorted(cats["undefined"]):
                    if app not in self.config.get("_hidden", []):
                        print(app)

        elif cmd == "find" or cmd == "search":
            for filename in os.listdir(self.getAppDir()):
                if filename.lower().startswith(args[1]): 
                    print(filename)
        elif cmd == "register_launcher":
            if len(args) < 4: return GPMLogger.error("Not enough options. (<extension> <name> <executable>)")
            ext = args[1]
            name = args[2]
            exec = args[3]
            self.config["launchers"][ext] = {"name": name, "executable": exec}
            GPMLogger.info(f"Registering {ext} as {name},{exec}")
            self.setConfig()

        elif cmd == "show_launchers":
            for k, v in self.config["launchers"].items():
                GPMLogger.info(f"{k} = {v['name']}, {v['executable']}")

        elif cmd == "hide":
            if not self.config.get("_hidden"): self.config["_hidden"] = []
            grp = " ".join(args[1:])
            if grp == "":
                for hidden in self.config["_hidden"]:
                    print(hidden)
                return

            if grp in self.config["_hidden"]:
                self.config["_hidden"].remove(grp)
                GPMLogger.info("Removed from hidden.")
            else:
                self.config["_hidden"].append(grp)
                GPMLogger.info("Added to hidden.")
            self.setConfig()
        elif cmd == "help":
            if len(args) >= 2:
                if args[1] == "set":
                    pass
                elif args[1] == "get":
                    pass
                elif args[1] == "install":
                    pass
                elif args[1] == "show_launchers":
                    pass
                elif args[1] == "register_launcher":
                    pass
                else:
                    for filename in os.listdir(gpm_root+"/scripts"):
                        if filename.endswith(".py") and args[1] == filename.split(".")[0]: 
                            os.chdir(gpm_root+"/scripts")
                            module = importlib.import_module("scripts."+filename.split(".")[0])
                            try:
                                return GPMLogger.info(module.help_gpm)
                            except:
                                return GPMLogger.info("There is no help_gpm field for this module.")
                    GPMLogger.error("No command found.")
            else:
                GPMLogger.info("install")
                GPMLogger.info("set")
                GPMLogger.info("get")
                GPMLogger.info("list")
                GPMLogger.info("find")
                GPMLogger.info("hide")
                GPMLogger.info("register_launcher")
                GPMLogger.info("show_launchers")
                for filename in os.listdir(gpm_root+"/scripts"):
                    os.chdir(gpm_root+"/scripts")
                    if filename.endswith(".py"): 
                        GPMLogger.info(Fore.BLUE+filename.split(".")[0])
        else:
            for filename in os.listdir(gpm_root+"/scripts"):
                if filename.endswith(".py") and cmd == filename.split(".")[0]: 
                    os.chdir(gpm_root+"/scripts")
                    module = importlib.import_module("scripts."+filename.split(".")[0])
                    module.handle_gpm(" ".join(args[1:]), logger = GPMLogger)
if __name__ == "__main__":
    gpm = GPM()
    gpm.main(sys.argv[1:])