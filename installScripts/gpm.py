
import os

def installApp(path, **data):
    print("Install GPM in to user path? (Allowing command `gpm`)")
    r = input("(y/n) > ")
    if r == "y":
        launcher = input("Enter your python executable command (default: python3) > ")
        if launcher == "": launcher = "python3"
        bin_path = input("Enter your bin path (default: ~/.local/bin/) > ")
        if bin_path == "": bin_path = os.path.expanduser("~")+"/.local/bin/"
        if not bin_path.endswith("/"): bin_path = bin_path+"/"
        data["logger"].info("Starting registration...")
        data["logger"].info("Executable: "+launcher)
        data["logger"].info("Bin path: "+bin_path)
        scriptDir = data["scriptDir"]
        launcher_script = f"{launcher} {scriptDir} $@"
        with open(bin_path+"gpm", "w+") as f:
            f.write(launcher_script)
        print("Attempting to make file executable, if this fails, run `chmod +x "+bin_path+"gpm` manually.")
        os.system("chmod +x "+bin_path+"gpm")
